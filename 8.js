var fs = require("fs");
var eight = require('./');

fs.readFile("./inputs/8", function (err, data) {
  if (err) {
    throw err;
  }

  var commands = data.toString().split("\n");
  commands.pop();

  const screen = eight.executeCommands(50, 6, commands);
  const numberOfLitPixels = eight.countLitPixels(screen);

  eight.logScreen(screen);
  console.log("numberOfLitPixels: ", numberOfLitPixels);
});

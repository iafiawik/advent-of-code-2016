var fs = require("fs");

fs.readFile("./inputs/6", function (err, data) {
  if (err) {
    throw err;
  }

  var messages = splitMessages(data);
  var leastCommonLetters = countLetters((a, b) => a.count - b.count);
  var mostCommonLetters = countLetters((a, b) => b.count - a.count);

  console.log("Most common letters are: ", mostCommonLetters.map((letter) =>  letter.character).join(""));
  console.log("Least common letters are: ", leastCommonLetters.map((letter) =>  letter.character).join(""));

  function splitMessages(data) {
    var messages = data.toString().split("\n");
    messages.pop();

    return messages.map((message) => message.split(""));
  }

  function countLetters(sortFunc) {
    var mostCommonLetters = [];

    for (var i = 0; i < messages[0].length; i++) {
      mostCommonLetters.push(countLettersInColumn(i, sortFunc));
    }

    return mostCommonLetters;
  }

  function countLettersInColumn(columnIndex, sortFunc) {
    var letters = {};

    messages.forEach((message) => {
      var letter = message[columnIndex];
      var letterCount = letters[letter];

      if (letterCount >= 0) {
        letterCount++;
      } else {
        letterCount = 0;
      }

      letters[letter] = letterCount;
    });

    return Object.keys(letters).map((letter) => {
      return { character: letter, count: letters[letter] };
    })
    .sort(sortFunc)
    .shift();
  }
});

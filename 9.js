var fs = require("fs");
var nine = require('./');

fs.readFile("./inputs/9", function (err, data) {
  if (err) {
    throw err;
  }

  var input = data.toString();
  input = input.substring(0, input.length - 1)

  var decompressedVersion1 = nine.decompressString(input)
  var decompressedVersion2 = nine.decompressStringVersion2(input)

  console.log("Length of decompressed input (version 1) is: ", decompressedVersion1.length);
  console.log("Length of decompressed input (version 2) is: ", decompressedVersion2);
});

var md5 = require('md5');
var roomId = "ugkcyxxp";
var password = ["_", "_", "_", "_", "_", "_", "_", "_"];
var lastIndex = 0;

// Task 2
while (password.some((letter) => letter === "_")) {
  var nextCharacter = findNextCharacter(lastIndex);

  if (isValidPosition(nextCharacter.position)) {
    password[nextCharacter.position] = nextCharacter.character;

    console.log("Password is now: ", password);
  }

  lastIndex = nextCharacter.index + 1;
}

console.log("Password is: ", password.join(""));

function findNextCharacter(index) {
  var isValid = false;

  while (!isValid) {
    var hash = md5(roomId + index);

    if (index % 100000 === 0) {
      console.log("index: ", index, ", hash: ", hash);
    }

    if (hash.substring(0, 5) === "00000") {
      isValid = true;
      const position = hash.substring(5, 6);
      const nextCharacter = hash.substring(6, 7);
    }
    else {
      index++;
    }
  }

  return { character: nextCharacter, index, position };
}

function isValidPosition(position) {
  var isValid = true;

  if (position > password.length) {
    isValid = false;
  }

  if (password[position] !== "_") {
    isValid = false;
  }

  return isValid;
}

// // Task 1

//
// for (var i = 0; i < 8; i++) {
//   var nextCharacter = findNextCharacter(lastIndex);
//
//   password += nextCharacter.character;
//   console.log("nextCharacter", nextCharacter);
//   console.log("Password is now: ", password);
//
//   lastIndex = nextCharacter.index + 1;
// }

// function findNextCharacter(index) {
//   var isValid = false;
//
//   while (!isValid) {
//     var hash = md5(roomId + index);
//
//     if (index % 100000 === 0) {
//       console.log("index: ", index, ", hash: ", hash);
//     }
//
//     if (hash.substring(0, 5) === "00000") {
//       isValid = true;
//       const nextCharacter = hash.substring(5, 6);
//     }
//     else {
//       index++;
//     }
//   }
//
//   return { character: nextCharacter, index };
// }

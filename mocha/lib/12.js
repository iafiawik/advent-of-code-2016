var twelve = exports;

twelve.sayHello = function() {
  return "hello";
};

twelve.parseInstruction = function(string) {
  var splitted = string.split(" ");
  var commandName = splitted[0];
  var command = {};

  if (commandName === "cpy") {
    commandName = "copy";

    var isRegisterValue = isNaN(splitted[1]);
    var value = isRegisterValue ? splitted[1] : parseInt(splitted[1]);

    return {
      commandName,
      isRegisterValue,
      value,
      destination: splitted[2]
    }
  }
  else if (commandName === "inc" || commandName === "dec") {
    commandName = commandName === "inc" ? "increase" : "decrease";
    return {
      commandName,
      target: splitted[1]
    };
  }
  else if (commandName === "jnz") {
    commandName = "jump";
    var offset = parseInt(splitted[2]);

    var isRegisterValue = isNaN(splitted[1]);
    var value = isRegisterValue ? splitted[1] : parseInt(splitted[1]);

    return {
      commandName,
      isRegisterValue,
      offset,
      value
    }
  }
};

twelve.translateRegisterKeyToIndex = function(key) {
  if (key === "a") {
    return 0;
  }
  else if (key === "b"){
    return 1;
  }
  else if (key === "c") {
    return 2;
  }
  else if (key === "d") {
    return 3;
  }
};

twelve.parseInstructions = function(instructions) {
  return instructions.map((instruction) => {
    return this.parseInstruction(instruction);
  })
};

twelve.executeInstructions = function(instructions, registers) {
  instructions = this.parseInstructions(instructions);
  var currentInstructionIndex = 0;

  while(currentInstructionIndex <= instructions.length - 1) {
    var instruction = instructions[currentInstructionIndex];

    if (instruction.commandName === "copy") {
      var value;

      if (instruction.isRegisterValue) {
        value = registers[this.translateRegisterKeyToIndex(instruction.value)];
      }
      else {
        value = instruction.value;
      }

      registers[this.translateRegisterKeyToIndex(instruction.destination)] = value;

      currentInstructionIndex++;
    }
    else if (instruction.commandName === "increase") {
      registers[this.translateRegisterKeyToIndex(instruction.target)]++;

      currentInstructionIndex++;
    }
    else if (instruction.commandName === "decrease") {
      registers[this.translateRegisterKeyToIndex(instruction.target)]--;

      currentInstructionIndex++;
    }
    else if (instruction.commandName === "jump") {
      var value;

      if (instruction.isRegisterValue) {
        value = registers[this.translateRegisterKeyToIndex(instruction.value)];
      }
      else {
        value = instruction.value;
      }

      if (value !== 0) {
        currentInstructionIndex = currentInstructionIndex + instruction.offset;
      }
      else {
        currentInstructionIndex++;
      }
    }
  }

  return registers;
};

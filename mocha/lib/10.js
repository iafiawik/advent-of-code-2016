var ten = exports;

ten.sayHello = function() {
  return "hello";
};

ten.parseInstructions = function(instructions) {
	var giveInstructions = [];
	var botInstructions = [];

	instructions.forEach((instruction) => {
		if (this.isGiveInstruction(instruction)) {
			giveInstructions.push(this.parseGiveInstruction(instruction));
		}
		else {
			botInstructions.push(this.parseBotInstruction(instruction));
		}
	});

	return {
		giveInstructions,
		botInstructions
	};
};

ten.isGiveInstruction = function(string) {
	return string.substring(0, 5) == "value";
};

ten.parseGiveInstruction = function(string) {
	var content = string.match(/\d+/g);
	return {
		value: parseInt(content[0]),
		botId: parseInt(content[1])
	}
};

ten.parseBotInstruction = function(string) {
	var content = string.split(" ");

	var low = {
		toBot: content[5] === "bot",
		id: parseInt(content[6])
	};

	var high = {
		toBot: content[10] === "bot",
		id: parseInt(content[11])
	};

	return {
		botId: parseInt(content[1]),
		low,
		high
	}
};

ten.getEmptyBot = function(id) {
	return {
		id,
		chips: []
	};
}

ten.getEmptyOutput = function(id) {
	return {
		id: id,
		chips: []
	};
}

ten.assignValueToBot = function(bot, value) {
	if (bot.chips.length >= 2) {
		throw new Error("Bot has more than two chips!");
	}
	else {
		bot.chips.push(value);
		bot.chips.sort((a, b) => (a - b));
		return bot;
	}
};

ten.assignValueToOutput = function(output, value) {
	output.chips.push(value);
	output.chips.sort((a, b) => (a - b));
	return output;
};

ten.assignStartValuesToBots = function(bots, giveInstructions) {
	giveInstructions.forEach((instruction) => {
		var botId = instruction.botId;

		if (!bots[botId]) {
			bots[botId] = this.getEmptyBot(botId);
		}

		var bot = bots[botId];
		bot = this.assignValueToBot(bot, instruction.value);
	});

	return bots;
}

ten.assignInstructionsToBots = function(botInstructions) {
	var bots = {};

	botInstructions.forEach((instruction) => {
		var botId = instruction.botId;
		instruction.chips = [];

		if (!bots[botId]) {
			bots[botId] = instruction;
		}
	});

	return bots;
}

ten.getBotsWithTwoChips = function(bots) {
	return Object.keys(bots).map((botId) => {
		return bots[botId];
	})
	.filter((bot) => {
		return bot.chips.length === 2;
	});
}

ten.isResponsible = function(bot, compareValueA, compareValueB) {
	if (bot.chips.length === 2) {
		if (bot.chips[0] === compareValueA && bot.chips[1] === compareValueB) {
			return true;
		}

		if (bot.chips[1] === compareValueA && bot.chips[0] === compareValueB) {
			return true;
		}
	}

	return false;
};

ten.executeInstructions = function(instructions, compareValueA, compareValueB) {
	instructions = this.parseInstructions(instructions);
	var bots = this.assignInstructionsToBots(instructions.botInstructions);
	bots = this.assignStartValuesToBots(bots, instructions.giveInstructions);
	var outputs = {};
	var responsible;

	while(this.getBotsWithTwoChips(bots).length > 0) {
		this.getBotsWithTwoChips(bots).forEach((botWithTwoChips) => {

			if (this.isResponsible(botWithTwoChips, compareValueA, compareValueB)) {
				responsible = botWithTwoChips;
			}

			var lowerValueChip = botWithTwoChips.chips[0];
			var higherValueChip = botWithTwoChips.chips[1];
			var lowerReceiverId = botWithTwoChips.low.id;
			var higherReceiverId = botWithTwoChips.high.id;


			if (botWithTwoChips.low.toBot) {
				if (!bots[lowerReceiverId]) {
					bots[lowerReceiverId] = this.getEmptyBot(lowerReceiverId);
				}

				bots[lowerReceiverId] = this.assignValueToBot(bots[lowerReceiverId], lowerValueChip);
			}
			else {
				if (!outputs[lowerReceiverId]) {
					outputs[lowerReceiverId] = this.getEmptyOutput(lowerReceiverId);
				}

				outputs[lowerReceiverId] = this.assignValueToOutput(outputs[lowerReceiverId], lowerValueChip);
			}

			if (botWithTwoChips.high.toBot) {
				if (!bots[higherReceiverId]) {
					bots[higherReceiverId] = this.getEmptyBot(higherReceiverId);
				}

				this.assignValueToBot(bots[higherReceiverId], higherValueChip);
			}
			else {
				if (!outputs[higherReceiverId]) {
					outputs[higherReceiverId] = this.getEmptyOutput(higherReceiverId);
				}

				outputs[higherReceiverId] = this.assignValueToOutput(outputs[higherReceiverId], higherValueChip);
			}

			botWithTwoChips.chips = [];
		})
	}

	return {
		bots,
		outputs,
		responsible
	};
}

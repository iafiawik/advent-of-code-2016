var seven = exports;
seven.sayHello = function() {
  return "hello";
};

seven.isAbba = function(string) {
 const reversedString = string.split("").reverse().join("");

 if (string === reversedString) {
   const splittedString = string.split("");

   return !splittedString.every((letter)=> letter === splittedString[0]);
 }

 return false;
};

seven.isAba = function(string) {
  return string.charAt(0) === string.charAt(2) && string.charAt(1) !== string.charAt(0);
};

seven.isCorrespondingBab = function(aba, bab) {
  const correctBab = aba.charAt(1) + aba.charAt(0) + aba.charAt(1);

  if (bab === correctBab) {
    return seven.isAba(bab) && bab !== aba;
  }
  else {
    return false;
  }
};

seven.getStringsInsideBrackets = function(string) {
  return string.match(/[^[\]]+(?=])/g);
}

seven.getStringsOutsideBrackets = function(string) {
  return string.match(/([^[\]]+)(?=$|\[)/g);
}

seven.getPossibleStringsInsideString = function(string, stringLength) {
  var strings = [];
  const splittedString = string.split("");

  for (var j = 0; j < splittedString.length - 1; j++) {
    var result = "";

    for (var i = 0; i < stringLength; i++) {
      result += splittedString[i + j];
    }

    if (result.length === stringLength) {
      strings.push(result);
    }
  }

  return strings;
}

seven.stringSupportsTls = function(insideBrackets, outsideBrackets) {
  const anyStringInsideBracketIsAbba = insideBrackets.some((string) => {
    return this.isAbba(string);
  });

  if (!outsideBrackets) {
    return anyStringInsideBracketIsAbba;
  }
  else {
    const anyStringOutsideBracketIsAbba = outsideBrackets.some((string) => {
      return this.isAbba(string);
    });
  }

  return anyStringOutsideBracketIsAbba && !anyStringInsideBracketIsAbba;
}

seven.stringSupportsSsl = function(insideBrackets, outsideBrackets) {
  const isAbaAndOutsideBrackets = outsideBrackets.filter((string) => {
    return this.isAba(string);
  });

  return isAbaAndOutsideBrackets.some((aba) => {
    return insideBrackets.filter((string) => {
      return this.isAba(string);
    })
    .some((bab) => {
      return seven.isCorrespondingBab(aba, bab);
    });
  });
}

seven.getPossibleInsideAndOutsideStrings = function(string, sequenceLength) {
  const insideBracketStrings = this.getStringsInsideBrackets(string);
  const outsideBracketStrings = this.getStringsOutsideBrackets(string);

  var possibleStringsInsideBrackets = [];
  var possibleStringsOutsideBrackets =[];

  insideBracketStrings.forEach((string) => {
    possibleStringsInsideBrackets = possibleStringsInsideBrackets.concat(this.getPossibleStringsInsideString(string, sequenceLength));
  });

  outsideBracketStrings.forEach((string) => {
    possibleStringsOutsideBrackets = possibleStringsOutsideBrackets.concat(this.getPossibleStringsInsideString(string, sequenceLength));
  });

  return {
    possibleStringsOutsideBrackets,
    possibleStringsInsideBrackets
  }
}

seven.isValidTlsString = function(string) {
  const possibleStrings = seven.getPossibleInsideAndOutsideStrings(string, 4);
  return this.stringSupportsTls(possibleStrings.possibleStringsInsideBrackets, possibleStrings.possibleStringsOutsideBrackets);
}

seven.isValidSslString = function(string) {
  const possibleStrings = seven.getPossibleInsideAndOutsideStrings(string, 3);
  return this.stringSupportsSsl(possibleStrings.possibleStringsInsideBrackets, possibleStrings.possibleStringsOutsideBrackets);
}

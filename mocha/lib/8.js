var eight = exports;

eight.sayHello = function() {
  return "hello";
};

eight.parseCommand = function(string) {
  var command = {};
  var splitted = string.split(" ");
  command.commandName = splitted[0];

  if (command.commandName === "rect") {
    var dimensions = splitted[1].split("x");

    command.rectWidth = parseInt(dimensions[0])
    command.rectHeight = parseInt(dimensions[1]);
  } else if (command.commandName === "rotate") {
    command.direction = splitted[1];
    command.numberOfPixels = parseInt(splitted[splitted.length - 1]);

    const position = parseInt(splitted[2].match(/=(.*)/)[1]);

    if (command.direction === "row") {
      command.y = position;
    }
    else if (command.direction === "column") {
      command.x = position;
    }
  }

  return command;
};

eight.createScreen = function(width, height) {
  var result = [];

  for (var i = 0; i < height; i++) {
    result[i] = [];

    for (var j = 0; j < width; j++) {
      result[i].push(".");
    }
  }

  return result;
};

eight.lightUpPixel = function(screen, x, y) {
  screen[x][y] = "#";

  return screen;
};

eight.logScreen = function(screen) {
  var logMessage = "";
  for (var i = 0; i < screen.length; i++) {
    for (var j = 0; j < screen[i].length; j++) {
      logMessage += screen[i][j];
    }

    logMessage += "\n";
  }

  console.log("Screen:\n\n" + logMessage);
}

eight.createRect = function(screen, rectWidth, rectHeight) {
  for (var j = 0; j < rectHeight; j++) {
    for (var i = 0; i < rectWidth; i++) {
      screen = this.lightUpPixel(screen, j, i);
    }
  }

  return screen;
};

eight.rotateRow = function(screen, y, pixels) {

  for (var i = 0; i < pixels; i++) {
    var previousValue = undefined;

    for (var j = 0; j < screen[y].length; j++) {

      if (!previousValue) {
        previousValue = screen[y][screen[y].length - 1];
      }

      var currentValue = screen[y][j];

      screen[y][j] = previousValue;
      previousValue = currentValue;
    }
  }

  return screen;
};

eight.rotateColumn = function(screen, x, pixels) {
  for (var i = 0; i < pixels; i++) {
    var previousValue = undefined;

    for (var j = 0; j < screen.length; j++) {
      if (!previousValue) {
        previousValue = screen[screen.length - 1][x];
      }

      var currentValue = screen[j][x];

      screen[j][x] = previousValue;
      previousValue = currentValue;
    }
  }

  return screen;
};

eight.countLitPixels = function(screen) {
  var numberOfLitPixels = 0;

  for (var i = 0; i < screen.length; i++) {
    for (var j = 0; j < screen[i].length; j++) {
      if (screen[i][j] === "#") {
        numberOfLitPixels++;
      }
    }
  }

  return numberOfLitPixels;
};

eight.executeCommand = function(screen, command) {
  if (command.commandName === "rect") {
    return this.createRect(screen, command.rectWidth, command.rectHeight);
  }
  else if (command.commandName === "rotate") {
    if (command.direction === "row") {
      return this.rotateRow(screen, command.y, command.numberOfPixels);
    }
    else if (command.direction === "column") {
      return this.rotateColumn(screen, command.x, command.numberOfPixels);
    }
  }
};

eight.executeCommands = function(width, height, commands) {
  var screen = this.createScreen(width, height);

  commands.forEach((string) => {
    const command = this.parseCommand(string);

    screen = this.executeCommand(screen, command);
  });

  return screen;
};

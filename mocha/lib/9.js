var nine = exports;

nine.sayHello = function() {
  return "hello";
};

nine.parseMarker = function(string) {
  var content = string.match(/\d+/g);

  return {
    characters: parseInt(content[0]),
    repeats: parseInt(content[1])
  };
};

nine.getNextMarkerInString = function(string, startIndex) {
  var openingParentheseIndex = string.indexOf("(", startIndex);
  var closingParentheseIndex = string.indexOf(")", openingParentheseIndex + 1);

  if (openingParentheseIndex >= 0 && closingParentheseIndex >= 0) {
    var markerString = string.substring(openingParentheseIndex, (closingParentheseIndex + 1));
    var marker = this.parseMarker(markerString);

    return {
      marker: marker,
      openingParentheseIndex,
      closingParentheseIndex
    }
  }
};

nine.decompressString = function(string) {
  var isFinished = false;
  var lastClosingIndex = 0;

  while(!isFinished) {
    var splitted = string.split("");
    var marker = this.getNextMarkerInString(string, lastClosingIndex);

    if (!marker) {
      isFinished = true;
    }
    else {

      var markerLength = marker.closingParentheseIndex - marker.openingParentheseIndex + 1;
      var charactersToRepeat = splitted.slice((marker.closingParentheseIndex + 1), (marker.closingParentheseIndex + 1 + marker.marker.characters)).join("");

      var charactersToInsert = "";

      for (var i = 0; i < marker.marker.repeats - 1; i++) {
        charactersToInsert += charactersToRepeat;
      }

      if (marker.marker.repeats ===1) {
        charactersToInsert = charactersToRepeat;
        markerLength = markerLength + charactersToRepeat.length;
      }

      lastClosingIndex = marker.openingParentheseIndex + marker.marker.characters * marker.marker.repeats;
      splitted.splice(marker.openingParentheseIndex, markerLength, charactersToInsert);

      string = splitted.join("");
    }
  }

  return string;
};

nine.decompressStringVersion2 = function(string) {
  var isFinished = false;
  var lastClosingIndex = 0;
  var markerLength;
	var finalStringLength = 0;

  var charactersToRepeat;
  var charactersToInsert;

  var marker;
	var firstParentheseIndex = string.match(/[(]/).index;

	if(firstParentheseIndex > 0) {
		var firstCharacters = string.substring(0, firstParentheseIndex);
		finalStringLength = firstCharacters.length;

		string = string.substring(firstParentheseIndex);
	}

  while(!isFinished) {
    marker = this.getNextMarkerInString(string, lastClosingIndex);

    if (!marker) {
			finalStringLength = finalStringLength + string.length;
      isFinished = true;
    }
    else {
      markerLength = marker.closingParentheseIndex - marker.openingParentheseIndex + 1;

      charactersToRepeat = string.substring((marker.closingParentheseIndex + 1), (marker.closingParentheseIndex + 1 + marker.marker.characters));
      charactersToInsert = "";

      for (var i = 0; i < marker.marker.repeats; i++) {
        charactersToInsert += charactersToRepeat;
      }

      var lastPart = string.substring(marker.closingParentheseIndex + 1 + marker.marker.characters);

			if (!charactersToInsert.match(/[(]/)) {
				finalStringLength = finalStringLength + charactersToInsert.length + marker.openingParentheseIndex;
				string = lastPart;
			}
			else {
				string = charactersToInsert + lastPart;
				finalStringLength = finalStringLength + marker.openingParentheseIndex;
			}
    }
  }

  return finalStringLength;
};

var twelve = require('../../');
var assert = require('assert');
var should = require('should');

describe("--- Day 12: Leonardo's Monorail ---", function(){
  describe('#sayHello()', function(){
    it('should say hello', function(){
      twelve.sayHello().should.equal("hello");
    })
  });

  describe('#parseInstruction()', function(){
    it('should parse a copy instruction', function(){
      var result = twelve.parseInstruction("cpy 1 a");

      result.commandName.should.equal("copy");
      result.isRegisterValue.should.equal(false);
      result.value.should.equal(1);
    });

    it('should parse a decrease instruction', function(){
      var result = twelve.parseInstruction("dec c");

      result.commandName.should.equal("decrease");
      result.target.should.equal("c");
    });

    it('should parse an increase instruction', function(){
      var result = twelve.parseInstruction("inc a");

      result.commandName.should.equal("increase");
      result.target.should.equal("a");
    });

    it('should parse a jump instruction', function(){
      var result = twelve.parseInstruction("jnz c -5");

      result.commandName.should.equal("jump");
      result.value.should.equal("c");
      result.isRegisterValue.should.equal(true);
      result.offset.should.equal(-5);
    });
  });

  describe('#parseInstructions()', function(){
    it('should parse multiple instructions', function(){

      var instructions = [
        "cpy 41 a",
        "inc a",
        "inc a",
        "dec a",
        "jnz a 2",
        "dec a"
      ];

      var result = twelve.parseInstructions(instructions);

      result.length.should.equal(instructions.length);
      result[0].commandName.should.equal("copy");
      result[result.length - 1].commandName.should.equal("decrease");
    })
  });

  describe('#executeInstructions()', function(){
    it('should execute instructions', function(){

      var instructions = [
        "cpy 41 a",
        "inc a",
        "inc a",
        "dec a",
        "jnz a 2",
        "dec a"
      ];

      var result = twelve.executeInstructions(instructions, [0, 0, 0, 0]);

      result[0].should.equal(42);
    })
  });
});

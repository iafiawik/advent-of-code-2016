var ten = require('../../');
var assert = require('assert');
var should = require('should');

describe('--- Day 10: Balance Bots ---', function(){
  describe('#sayHello()', function(){
    it('should say hello', function(){
      ten.sayHello().should.equal("hello");
    })
  });

  describe('#isGiveInstruction()', function(){
    it('should detect valid give instructions', function(){
      ten.isGiveInstruction("value 67 goes to bot 187").should.equal(true);
    });

    it('should detect invalid give instructions', function(){
      ten.isGiveInstruction("bot 140 gives low to bot 206 and high to bot 189").should.equal(false);
    })
  });

  describe('#parseGiveInstruction()', function(){
    it('should correctly parse a give instruction', function(){
      var result = ten.parseGiveInstruction("value 67 goes to bot 187");

      result.value.should.equal(67);
      result.botId.should.equal(187);
    });
  });

  describe('#parseBotInstruction()', function(){
    it('should correctly parse a bot to bot instruction', function(){
      var result = ten.parseBotInstruction("bot 140 gives low to bot 206 and high to bot 189");

      result.botId.should.equal(140);
      result.low.toBot.should.equal(true);
      result.low.id.should.equal(206);
      result.high.toBot.should.equal(true);
      result.high.id.should.equal(189);
    });

    it('should correctly parse a bot to output instruction', function(){
      var result = ten.parseBotInstruction("bot 1 gives low to output 1 and high to bot 0");

      result.botId.should.equal(1);
      result.low.toBot.should.equal(false);
      result.low.id.should.equal(1);
      result.high.toBot.should.equal(true);
      result.high.id.should.equal(0);
    });
  });

  describe('#parseInstructions()', function(){
    it('should correctly parse a list of instructions', function(){

      var instructions =
        ["value 67 goes to bot 187",
        "value 31 goes to bot 97",
        "value 29 goes to bot 86",
        "value 73 goes to bot 12",
        "value 47 goes to bot 36",
        "value 3 goes to bot 29",
        "value 43 goes to bot 206",
        "value 71 goes to bot 89",
        "value 59 goes to bot 207",
        "value 41 goes to bot 136",
        "value 37 goes to bot 43",
        "value 13 goes to bot 103",
        "value 2 goes to bot 57",
        "value 19 goes to bot 151",
        "value 61 goes to bot 2",
        "value 5 goes to bot 140",
        "value 11 goes to bot 121",
        "value 53 goes to bot 197",
        "value 23 goes to bot 97",
        "value 7 goes to bot 40",
        "value 17 goes to bot 32"];

      var instructions = ten.parseInstructions(instructions);

      instructions.giveInstructions.length.should.equal(21);
      instructions.giveInstructions[0].value.should.equal(67);
    });
  });

  describe('#assignStartValuesToBots()', function(){
    it('should correctly assign start values to bots', function(){

      var instructions =
        ["value 67 goes to bot 187",
        "value 31 goes to bot 97",
        "value 29 goes to bot 86",
        "value 73 goes to bot 12",
        "value 47 goes to bot 36",
        "value 3 goes to bot 29",
        "value 43 goes to bot 206",
        "value 71 goes to bot 89",
        "value 59 goes to bot 207",
        "value 41 goes to bot 136",
        "value 37 goes to bot 43",
        "value 13 goes to bot 103",
        "value 2 goes to bot 57",
        "value 19 goes to bot 151",
        "value 61 goes to bot 2",
        "value 5 goes to bot 140",
        "value 11 goes to bot 121",
        "value 53 goes to bot 197",
        "value 23 goes to bot 97",
        "value 7 goes to bot 40",
        "value 17 goes to bot 32"];

      var bots = ten.assignStartValuesToBots({}, ten.parseInstructions(instructions).giveInstructions);

      bots["187"].chips[0].should.equal(67);
      bots["97"].chips[0].should.equal(23);
      bots["32"].chips[0].should.equal(17);
    });
  });

  describe('#executeInstructions()', function(){
    it('should correctly execute instructions', function(){

      var instructions =
        ["value 5 goes to bot 2",
        "bot 2 gives low to bot 1 and high to bot 0",
        "value 3 goes to bot 1",
        "bot 1 gives low to output 1 and high to bot 0",
        "bot 0 gives low to output 2 and high to output 0",
        "value 2 goes to bot 2"];

      var result = ten.executeInstructions(instructions, 5, 2);
      console.log("result", result.responsible);
      result.bots["0"].chips.length.should.equal(0);
      result.bots["1"].chips.length.should.equal(0);
      result.bots["2"].chips.length.should.equal(0);

      result.outputs["0"].chips[0].should.equal(5);
      result.outputs["1"].chips[0].should.equal(2);
      result.outputs["2"].chips[0].should.equal(3);

      result.responsible.botId.should.equal(2);
    });
  });


  describe('#assignValueToBot()', function(){
    it('should throw error if a bot has more than two chips', function(){
      (function() {
        ten.assignValueToBot({ chips: [23, 24]}, 25)
      }).should.throw(Error);
    });

    it('should assign and sort newly added value', function(){
      var result = ten.assignValueToBot({ chips: [24]}, 13);
      result.chips.length.should.equal(2);
      result.chips[0].should.equal(13);

      var result = ten.assignValueToBot({ chips: []}, 13);
      result.chips.length.should.equal(1);
      result.chips[0].should.equal(13);

      var result = ten.assignValueToBot({ chips: [13]}, 24);
      result.chips.length.should.equal(2);
      result.chips[0].should.equal(13);
    });
  });

  describe('#isResponsible()', function(){
    it('should detect a responsible bot', function(){
      var result = ten.isResponsible({ chips: [13, 24]}, 13, 24);
      result.should.equal(true);

      var result = ten.isResponsible({ chips: [24, 13]}, 13, 24);
      result.should.equal(true);
    });

    it('should detect a non responsible bot', function(){
      var result = ten.isResponsible({ chips: [13, 24]}, 13, 25);
      result.should.equal(false);

      var result = ten.isResponsible({ chips: []}, 13, 25);
      result.should.equal(false);

      var result = ten.isResponsible({ chips: [13]}, 13, 25);
      result.should.equal(false);
    });
  });
});

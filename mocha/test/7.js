var seven = require('../../');
var assert = require('assert');
var should = require('should');

describe('--- Day 7: Internet Protocol Version 7 ---', function(){
  describe('#sayHello()', function(){
    it('should say hello', function(){
      seven.sayHello().should.equal("hello");
    })
  });

  describe('#isAbba()', function(){
    it('should detect a valid abba', function(){
      seven.isAbba("abba").should.be.true();
    });

    it('should detect invalid abbas', function(){
      seven.isAbba("abab").should.be.false()
      seven.isAbba("aaaa").should.be.false()
      seven.isAbba("afdsaaa").should.be.false()
    });
  });

  describe('#isAba()', function(){
    it('should detect a valid aba', function(){
      seven.isAbba("aba").should.be.true();
      seven.isAbba("bab").should.be.true();
    });

    it('should detect invalid abas', function(){
      seven.isAbba("abab").should.be.false()
      seven.isAbba("aaa").should.be.false()
      seven.isAbba("afdsaaa").should.be.false()
    });
  });

  describe('#isCorrespondingBab()', function(){
    it('should detect a valid bab', function(){
      seven.isCorrespondingBab("aba", "bab").should.be.true();
      seven.isCorrespondingBab("bab", "aba").should.be.true();
    });

    it('should detect invalid bab', function(){
      seven.isCorrespondingBab("xyx", "xyy").should.be.false()
      seven.isAbba("aaa", "aaa").should.be.false()
      seven.isAbba("sdad", "sdad").should.be.false()
    });
  });

  describe('#getStringsInsideBrackets()', function(){
    it('should get a single text', function(){
      const result = seven.getStringsInsideBrackets("ioxxoj[asdfgh]zxcvbn")[0];

      result.should.equal("asdfgh");
    });

    it('should return null when there is no content between brackets', function(){
      const result = seven.getStringsInsideBrackets("ioxxoj[]zxcvbn");

      should.not.exist(result);
    });
  });

  describe('#getPossibleStringsInsideString()', function(){
    it('should get a single text (4 characters)', function(){
      const result = seven.getPossibleStringsInsideString("ioxx", 4);

      result.length.should.equal(1);
    });

    it('should get a single text (3 characters)', function(){
      const result = seven.getPossibleStringsInsideString("ixx", 3);

      result.length.should.equal(1);
    });

    it('should extract multiple strings (4 characters)', function(){
      const result = seven.getPossibleStringsInsideString("ioxxsds", 4);
      result.length.should.equal(4);
    });

    it('should not find any strings in short string (4 characters)', function(){
      const result = seven.getPossibleStringsInsideString("sds", 4);

      result.length.should.equal(0);
    });
  });

  describe('#isValidTlsString()', function(){
    it('should detect valid strings', function(){
      const result1 = seven.isValidTlsString("abba[mnop]qrst", 4);
      result1.should.be.true();

      const result2 = seven.isValidTlsString("ioxxoj[asdfgh]zxcvbn", 4);
      result2.should.be.true()
    });

    it('should detect invalid strings', function(){
      const result1 = seven.isValidTlsString("abcd[bddb]xyyx", 4);
      result1.should.be.false();

      const result2 = seven.isValidTlsString("aaaa[qwer]tyui", 4);
      result2.should.be.false()
    });
  });

  describe('#isValidSslString()', function(){
    it('should detect valid strings', function(){
      const result1 = seven.isValidSslString("aba[bab]xyz", 3);
      result1.should.be.true();

      const result2 = seven.isValidSslString("aaa[kek]eke", 3);
      result2.should.be.true();

      const result3 = seven.isValidSslString("zazbz[bzb]cdb", 3);
      result3.should.be.true();
    });

    it('should detect invalid strings', function(){
      const result1 = seven.isValidSslString("xyx[xyx]xyx", 3);
      result1.should.be.false();
    });
  });
})

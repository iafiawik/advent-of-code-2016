var eight = require('../../');
var assert = require('assert');
var should = require('should');

describe('--- Day 8: Two-Factor Authentication ---', function(){
  describe('#sayHello()', function(){
    it('should say hello', function(){
      eight.sayHello().should.equal("hello");
    })
  });

  describe('#parseCommand()', function(){
    it('should detect a rect command', function(){
      const command = eight.parseCommand("rect 2x1");

      command.commandName.should.equal("rect");
      command.rectWidth.should.equal(2);
      command.rectHeight.should.equal(1)
    });

    it('should detect a rotate row command', function(){
      const command = eight.parseCommand("rotate row y=10 by 2");

      command.commandName.should.equal("rotate");
      command.y.should.equal(10);
      command.direction.should.equal("row");
      command.numberOfPixels.should.equal(2)
    });

    it('should detect a rotate column command', function(){
      const command = eight.parseCommand("rotate column x=32 by 1");

      command.commandName.should.equal("rotate");
      command.x.should.equal(32);
      command.direction.should.equal("column");
      command.numberOfPixels.should.equal(1)
    });
  });

  describe('#createScreen()', function(){
    it('return correct number of pixels', function(){
      const width = 20;
      const height = 20;

      const result = eight.createScreen(width, height);

      (result[0].length * result.length).should.equal(height * width);
    });

    it('return correct number of pixels', function(){
      const width = 20;
      const height = 50;

      const result = eight.createScreen(width, height);

      (result[0].length * result.length).should.equal(height * width);
    })
  });

  describe('#lightUpPixel()', function(){
    it('should light up correct pixel', function(){
      const width = 11;
      const height = 11;
      var screen = eight.createScreen(width, height);

      screen = eight.lightUpPixel(screen, 0, 0);
      (screen[0][0]).should.equal("#");

      screen = eight.lightUpPixel(screen, 1, 1);
      (screen[1][1]).should.equal("#");

      screen = eight.lightUpPixel(screen, 10, 11);
      (screen[10][11]).should.equal("#");
    });
  });

  describe('#createRect()', function(){
    it('should react a rect with correct dimensions', function(){
      const width = 7;
      const height = 3;
      const screen = eight.createScreen(width, height);

      const rectWidth = 3;
      const rectHeight = 2;

      screen = eight.createRect(screen, rectWidth, rectHeight);
      (screen[0][0]).should.equal("#");
      (screen[0][1]).should.equal("#");
      (screen[0][2]).should.equal("#");
      (screen[1][0]).should.equal("#");
      (screen[1][1]).should.equal("#");
      (screen[1][2]).should.equal("#");
      (screen[2][1]).should.not.equal("#");
      (screen[2][2]).should.not.equal("#");
    });

    it('should react a rect with correct dimensions and count them', function(){
      const width = 7;
      const height = 3;
      const screen = eight.createScreen(width, height);

      const rectWidth = 5;
      const rectHeight = 3;

      screen = eight.createRect(screen, rectWidth, rectHeight);
      const numberOfLitPixels = eight.countLitPixels(screen);

      (screen[0][0]).should.equal("#");
      (screen[0][1]).should.equal("#");
      (screen[0][2]).should.equal("#");
      (screen[1][0]).should.equal("#");
      (screen[1][1]).should.equal("#");
      (screen[1][2]).should.equal("#");

      numberOfLitPixels.should.equal(15);
    });
  });

  describe('#countLitPixels()', function(){
    it('should count lit pixels', function(){
      const width = 7;
      const height = 3;
      const screen = eight.createScreen(width, height);

      const rectWidth = 3;
      const rectHeight = 2;

      screen = eight.createRect(screen, rectWidth, rectHeight);

      const numberOfLitPixels = eight.countLitPixels(screen);
      numberOfLitPixels.should.equal(6);
    })
  });

  describe('#rotateRow()', function(){
    it('rotate row 4 pixels of pixels', function(){
      const width = 7;
      const height = 3;
      const rectWidth = 3;
      const rectHeight = 2;

      var screen = eight.createScreen(width, height);
      screen = eight.createRect(screen, rectWidth, rectHeight);
      screen = eight.rotateRow(screen, 0, 4);

      (screen[0][4]).should.equal("#");
      (screen[0][5]).should.equal("#");
      (screen[0][6]).should.equal("#");
      (screen[1][0]).should.equal("#");
      (screen[1][1]).should.equal("#");
      (screen[1][2]).should.equal("#");

      (screen[2][0]).should.not.equal("#");
      (screen[2][1]).should.not.equal("#");
    });

    it('rotates row and handles falling off pixels', function(){
      const width = 7;
      const height = 3;
      const rectWidth = 3;
      const rectHeight = 2;

      var screen = eight.createScreen(width, height);
      screen = eight.createRect(screen, rectWidth, rectHeight);
      screen = eight.rotateRow(screen, 1, 5);

      (screen[1][0]).should.equal("#");
      (screen[1][5]).should.equal("#");
      (screen[1][6]).should.equal("#");
      (screen[0][0]).should.equal("#");
      (screen[0][1]).should.equal("#");
      (screen[0][2]).should.equal("#");

      (screen[2][0]).should.not.equal("#");
      (screen[2][1]).should.not.equal("#");
    });
  });

  describe('#rotateColumn()', function(){
    it('rotate column by 4', function(){
      const width = 7;
      const height = 7;
      const rectWidth = 3;
      const rectHeight = 2;

      var screen = eight.createScreen(width, height);
      screen = eight.createRect(screen, rectWidth, rectHeight);
      screen = eight.rotateColumn(screen, 1, 4);

      const numberOfLitPixels = eight.countLitPixels(screen);

      (screen[4][1]).should.equal("#");

      (screen[0][1]).should.not.equal("#");
      (screen[1][1]).should.not.equal("#");
      (screen[2][1]).should.not.equal("#");
      (screen[3][1]).should.not.equal("#");

      numberOfLitPixels.should.equal(6);
    });

    it('rotates and handles falling off pixels', function(){
      const width = 7;
      const height = 5;
      const rectWidth = 3;
      const rectHeight = 2;

      var screen = eight.createScreen(width, height);
      screen = eight.createRect(screen, rectWidth, rectHeight);
      screen = eight.rotateColumn(screen, 1, 4);
      const numberOfLitPixels = eight.countLitPixels(screen);

      (screen[0][1]).should.equal("#");
      (screen[4][1]).should.equal("#");

      (screen[1][1]).should.not.equal("#");
      (screen[2][1]).should.not.equal("#");
      (screen[3][1]).should.not.equal("#");

      numberOfLitPixels.should.equal(6);
    });
  });

  describe('#executeCommand()', function(){
    it('should execute create rect command', function(){
      const width = 7;
      const height = 3;
      var screen = eight.createScreen(width, height);

      const command = eight.parseCommand("rect 3x2");
      screen = eight.executeCommand(screen, command);

      (screen[0][0]).should.equal("#");
      (screen[0][1]).should.equal("#");
      (screen[0][2]).should.equal("#");
      (screen[1][0]).should.equal("#");
      (screen[1][1]).should.equal("#");
      (screen[1][2]).should.equal("#");
      (screen[2][1]).should.not.equal("#");
      (screen[2][2]).should.not.equal("#");
    });

    it('should execute rotate row command', function(){
      const width = 7;
      const height = 3;
      const rectWidth = 3;
      const rectHeight = 2;

      var screen = eight.createScreen(width, height);
      screen = eight.createRect(screen, rectWidth, rectHeight);

      const command = eight.parseCommand("rotate row y=0 by 4");
      screen = eight.executeCommand(screen, command);

      (screen[0][4]).should.equal("#");
      (screen[0][5]).should.equal("#");
      (screen[0][6]).should.equal("#");
      (screen[1][0]).should.equal("#");
      (screen[1][1]).should.equal("#");
      (screen[1][2]).should.equal("#");

      (screen[2][0]).should.not.equal("#");
    });

    it('should execute rotate column command', function(){
      const width = 7;
      const height = 7;
      const rectWidth = 3;
      const rectHeight = 2;

      var screen = eight.createScreen(width, height);
      screen = eight.createRect(screen, rectWidth, rectHeight);

      const command = eight.parseCommand("rotate column x=1 by 4");
      screen = eight.executeCommand(screen, command);

      const numberOfLitPixels = eight.countLitPixels(screen);

      (screen[4][1]).should.equal("#");

      (screen[0][1]).should.not.equal("#");
      (screen[1][1]).should.not.equal("#");
      (screen[2][1]).should.not.equal("#");
      (screen[3][1]).should.not.equal("#");

      numberOfLitPixels.should.equal(6);
    });
  });

  describe('#executeCommands()', function(){
    it('should end up with correct number of lit pixels', function(){
      const width = 7;
      const height = 3;
      const rectWidth = 3;
      const rectHeight = 2;
      var screen = eight.createScreen(width, height);
      screen = eight.createRect(screen, rectWidth, rectHeight);

      screen = eight.rotateColumn(screen, 1, 1);
      screen = eight.rotateRow(screen, 0, 4);
      screen = eight.rotateColumn(screen, 1, 1);
      const numberOfLitPixels = eight.countLitPixels(screen);

      numberOfLitPixels.should.equal(6);
    });

    it('should execute a number of string commands', function(){
      const width = 7;
      const height = 3;

      const commands = [
        "rect 3x2",
        "rotate column x=1 by 1",
        "rotate row y=0 by 4",
        "rotate column x=1 by 1"
      ];

      var screen = eight.executeCommands(width, height, commands);
    });
  });
})

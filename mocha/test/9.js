var nine = require('../../');
var assert = require('assert');
var should = require('should');

describe('--- Day 9: Explosives in Cyberspace ---', function(){
  describe('#sayHello()', function(){
    it('should say hello', function(){
      nine.sayHello().should.equal("hello");
    })
  });

  describe('#parseMarker()', function(){
    it('should parse a marker (string)', function(){

      var result = nine.parseMarker("(1x5)");

      result.characters.should.equal(1);
      result.repeats.should.equal(5);

      result = nine.parseMarker("(3x4)");
      result.characters.should.equal(3);
      result.repeats.should.equal(4);
    });

    it('should extract correct number of characters', function(){
      var result = nine.parseMarker("(8x5)");
      result.characters.should.equal(8);
    });

    it('should extract correct number of repeats', function(){

      var result = nine.parseMarker("(1x5)");
      result.repeats.should.equal(5);
    });
  });

  describe('#getNextMarkerInString()', function(){
    it('should correctly process a string', function(){
      var result = nine.getNextMarkerInString("A(1x5)BC", 0);

      console.log("result: ", result);
      result.marker.repeats.should.equal(5);
      result.marker.characters.should.equal(1);
      result.openingParentheseIndex.should.equal(1);
      result.closingParentheseIndex.should.equal(5);
    });

    it('should correctly process a string with marker at index 0', function(){
      var result = nine.getNextMarkerInString("(3x3)XYZ", 0);

      console.log("result: ", result);
      result.marker.repeats.should.equal(3);
      result.marker.characters.should.equal(3);
      result.openingParentheseIndex.should.equal(0);
      result.closingParentheseIndex.should.equal(4);
    });
  });
  //
  describe('#decompressStringVersion1()', function(){
    it('should correctly decompress a string with no marker', function(){
      var result = nine.decompressStringVersion1("ADVENT");
      console.log("result: ", result);

      result.should.equal("ADVENT");
    });

    it('should correctly decompressMarker a string with one marker', function(){
      var result = nine.decompressStringVersion1("A(1x5)BC");
      console.log("result: ", result);

      result.should.equal("ABBBBBC");

      var result2 = nine.decompressStringVersion1("(3x3)XYZ");
      console.log("result2: ", result2);
      result2.should.equal("XYZXYZXYZ");
    });

    it('should correctly decompress a string with multiple markers', function(){
      var result = nine.decompressStringVersion1("A(2x2)BCD(2x2)EFG");
      console.log("result: ", result);
      result.should.equal("ABCBCDEFEFG");
      result.length.should.equal(11);

      var result2 = nine.decompressStringVersion1("(6x1)(1x3)A");
      console.log("result2: ", result2);
      result2.should.equal("(1x3)A");

      var result2 = nine.decompressStringVersion1("X(8x2)(3x3)ABCY");
      console.log("result2: ", result2);
      result2.should.equal("X(3x3)ABC(3x3)ABCY");
    });
  });
});

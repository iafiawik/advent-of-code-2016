var fs = require("fs");

fs.readFile('./inputs/1', function (err, data) {
  if (err) {
    throw err;
  }

  const directions = data.toString().split(", ");
  var currentXPosition = 0;
  var currentYPosition = 0;
  var lastDirection = "up";

  var visitedSteps = [];

  directions.forEach((direction) => {
    var numberOfBlocks = parseInt(direction.substring(1));
    var direction = direction.charAt(0);

    var nextXPosition = currentXPosition;
    var nextYPosition = currentYPosition;

    if (direction === "R") {
      if (lastDirection === "up") {
        nextXPosition += numberOfBlocks;
        lastDirection = "right";
      }
      else if (lastDirection === "down") {
        nextXPosition -= numberOfBlocks;
        lastDirection = "left";
      }
      else if (lastDirection === "right") {
        nextYPosition -= numberOfBlocks;
        lastDirection = "down";
      }
      else if (lastDirection === "left") {
        nextYPosition += numberOfBlocks;
        lastDirection = "up";
      }
    }
    else if (direction === "L") {
      if (lastDirection === "up") {
        nextXPosition -= numberOfBlocks;
        lastDirection = "left";
      }
      else if (lastDirection === "down") {
        nextXPosition += numberOfBlocks;
        lastDirection = "right";
      }
      else if (lastDirection === "right") {
        nextYPosition += numberOfBlocks;
        lastDirection = "up";
      }
      else if (lastDirection === "left") {
        nextYPosition -= numberOfBlocks;
        lastDirection = "down";
      }
    }

    var steps = getStepsBetweenPoints([currentXPosition, currentYPosition], [nextXPosition, nextYPosition]);
    visitedSteps = visitedSteps.concat(steps);

    currentXPosition = nextXPosition;
    currentYPosition = nextYPosition;
  });

  function getFirstStepVisitedTwice() {
    var duplicates = [];
    visitedSteps.forEach((step, index) => {
      var hasBeenHereBefore;

      visitedSteps.forEach((visitedStep, visitedStepIndex) => {
        if (step[0] === visitedStep[0] && step[1] === visitedStep[1] && index !== visitedStepIndex) {
          hasBeenHereBefore = true;
        }
      });

      if (hasBeenHereBefore) {
        duplicates.push(step);
      }
    });

    return duplicates[0];
  }

  function getStepsBetweenPoints(startPoint, endPoint) {
    var hasMovedSideways = startPoint[0] !== endPoint[0];
    var steps;

    if (hasMovedSideways) {
      steps = getStepsBetweenHorizontalPoints(startPoint, endPoint);
    }
    else {
      steps = getStepsBetweenVerticalPoints(startPoint, endPoint);
    }

    return steps;
  }

  function getStepsBetweenHorizontalPoints(startPoint, endPoint) {
    var steps = [];
    var current = startPoint[0];

    while(current !== endPoint[0]) {
      steps.push([current, startPoint[1]]);

      current = updateStepValue(startPoint[0], endPoint[0], current);
    }

    return steps;
  }

  function getStepsBetweenVerticalPoints(startPoint, endPoint) {
    var steps = [];
    var current = startPoint[1];

    while(current !== endPoint[1]) {
      steps.push([startPoint[0], current]);

      current = updateStepValue(startPoint[1], endPoint[1], current);
    }

    return steps;
  }

  function updateStepValue(start, end, value) {
    if (start > end) {
      value--;
    }
    else {
      value++;
    }

    return value;
  }

  function calculateBlocksAway(x, y) {
    x = Math.abs(x);
    y = Math.abs(y);

    if (x === y) return x;

    return x + y;
  }

  var numberOfBlocksAway = calculateBlocksAway(currentXPosition, currentYPosition);
  var firstStepToVisitTwice = getFirstStepVisitedTwice();

  console.log(`Final position is ${numberOfBlocksAway} blocks away`);
  console.log(`First step to visit twice is ${calculateBlocksAway(firstStepToVisitTwice[0], firstStepToVisitTwice[1])} steps away`);
});

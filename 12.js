var fs = require("fs");
var twelve = require('./');

fs.readFile("./inputs/12", function (err, data) {
  if (err) {
    throw err;
  }

  var input = data.toString().split("\n");
  input.pop();

  var result1 = twelve.executeInstructions(input, [0, 0, 0, 0]);
  var result2 = twelve.executeInstructions(input, [0, 0, 1, 0]);

  console.log(`Part 1: register A: ${result1[0]}`);
  console.log(`Part 2: register A: ${result2[0]}`);
});

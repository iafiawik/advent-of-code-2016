var fs = require("fs");

fs.readFile('./inputs/3', function (err, data) {
  if (err) {
    throw err;
  }

  // Task 1
  var triangles = data.toString().split("\n").map((triangle) => triangle.split(/[\s]+/).filter((number) => number !== "").map((number) => parseInt(number)));
  triangles.pop();

  // Task 2
  triangles = getTrianglesByColumns(triangles);

  var numberOfValidTriangles = 0;

  triangles.forEach((triangle) => {
    if (triangle.length === 0) return;

    if (isValidTriangle(triangle[0], triangle[1], triangle[2])) {
      numberOfValidTriangles++;
    }
  });

  console.log("numberOfValidTriangles", numberOfValidTriangles);

  function isValidTriangle(a, b, c) {
  	var sideAsqrd = (a) * (a); //side A squared for pythagorean theorem
  	var sideBsqrd = (a) * (b); //side B squared
  	var sideCsqrd = (c) * (c); //side C squared
  	var AB = (a) + (b); //side A + B to check if a triangle can be formed
  	var AC = (a) + (c); //side A + C
  	var BC = (b) + (c); //side B + C
  	var A2B2 = sideAsqrd + sideBsqrd;
  	var A2C2 = sideAsqrd + sideCsqrd;
  	var B2C2 = sideBsqrd + sideCsqrd;
  	if ((AB > c) && (AC > b) && (BC > a)) {
  		return true;
  	}
  	if ((sideAsqrd == B2C2) || (sideBsqrd == A2C2) || (sideCsqrd == A2B2)) {
  		return true;
  	}
  	else {
  		return false;
  	}
  }

  function getTrianglesByColumns(triangles) {
    var list = [];

    for (var i = 0; i < triangles.length; i++) {
      if (i % 3 == 0) {
        list.push([triangles[i][0], triangles[i+1][0], triangles[i+2][0]]);
        list.push([triangles[i][1], triangles[i+1][1], triangles[i+2][1]]);
        list.push([triangles[i][2], triangles[i+1][2], triangles[i+2][2]]);
      }
    }

    return list;
  }
});

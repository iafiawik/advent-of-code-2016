var fs = require("fs");
var ten = require('./');

fs.readFile("./inputs/10", function (err, data) {
  if (err) {
    throw err;
  }

  var input = data.toString().split("\n");
  input.pop();

  var result = ten.executeInstructions(input, 17, 61);
  var multipliedOutputs = result.outputs["0"].chips[0] * result.outputs["1"].chips[0] * result.outputs["2"].chips[0];

  console.log(`Bot ${result.responsible.botId} is responsible for comparing value-61 microchips with value-17 microchips`);
  console.log(`Multiplied outputs: ${multipliedOutputs}`);
});

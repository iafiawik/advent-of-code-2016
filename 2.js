var fs = require("fs");

fs.readFile('./inputs/2', function (err, data) {
  if (err) {
    throw err;
  }

  // Task 1
  // const keypad = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];

  // Task 2
  const keypad = [
    ["", "", 1, "", ""],
    ["", 2,3,4, ""],
    [5,6,7,8,9],
    ["", "A", "B", "C", ""],
    ["", "", "D", "", ""]
  ];

  const instructions = getInstructions(data.toString()).map((instruction) => getSteps(instruction));

  var codeNumbers = [];
  var currentPosition = { x: 0, y:2};

  instructions.forEach((instruction) => {
    if (instruction.length === 0) return;

    instruction.forEach((instruction) => {
      currentPosition = getNextCoordinate(currentPosition, instruction);
    });

    codeNumbers.push(getCodeNumber(currentPosition));
  });


  console.log("Code numbers are: ", codeNumbers.join(""));

  function getInstructions(rawData) {
    return rawData.split("\n");
  }

  function getSteps(rawData) {
    return rawData.split("");
  }

  function getCodeNumber(position) {
    return keypad[position.y][position.x];
  }

  function getNextCoordinate(currentPosition, instruction) {
    var x = currentPosition.x;
    var y = currentPosition.y;

    var nextCoordinate;
    if (instruction === "U" && y > 0) {
      nextCoordinate = { x, y: y - 1 };
    } else if (instruction === "D" && y < keypad.length - 1) {
      nextCoordinate = { x, y: y + 1 };
    }  else if (instruction === "R" && x < keypad[y].length - 1) {
      nextCoordinate = { x: x + 1, y };
    } else if (instruction === "L" && x > 0) {
      nextCoordinate = { x: x - 1, y };
    } else {
      nextCoordinate = { x, y };
    }

    if (validateCoordinate(nextCoordinate)) {
      return nextCoordinate;
    }
    else {
      return nextCoordinate = { x, y };
    }
  };

  function validateCoordinate(position) {
    return (keypad[position.y][position.x] !== "");
  }
});

var fs = require("fs");

fs.readFile('./inputs/4', function (err, data) {
  if (err) {
    throw err;
  }

  var sectorIdsSum = 0;
  var realNames = [];

  var rooms = data.toString().split("\n");
  rooms.pop();

  // Task 1
  rooms.forEach((room) => {
    const name = getName(room);

    if (isValidRoom(name)) {
      sectorIdsSum += name.sectorId;
    }
  });

  // Task 2
  rooms.forEach((room) => {
    realNames.push(getRealNameOfRoom(getName(room)))
  });

  const index = realNames.findIndex((realName) => {
    return realName === "northpole object storage";
  });

  console.log("Total checksum is: ", sectorIdsSum);
  console.log("Sector ID of North Pole objects is: ", getName(rooms[index]).sectorId);

  function getRealNameOfRoom(name) {
        var splittedName = name.name.split("");
    var finalName = "";

    for (var j = 0; j < splittedName.length; j++) {
      var letter = splittedName[j];

      for (var i = 0; i < name.sectorId; i++) {
        letter = getNextLetter(letter);
      }

      finalName += letter;
    }

    return finalName;
  }

  function getNextLetter(letter) {
    if (letter === "-") {
      return " ";
    }

    return letter.replace(/([a-zA-Z])[^a-zA-Z]*$/, function(a){
        var c= a.charCodeAt(0);
        switch(c){
            case 90: return 'A';
            case 122: return 'a';
            default: return String.fromCharCode(++c);
        }
    });
  }

  function isValidRoom(name) {
    const mostCommonLetters = getMostCommonLetters(name.letters);

    var letters = mostCommonLetters.map((letter) => letter.letter).join("");

    return letters === name.checksum;
  }

  function getName(room) {
    var name = room.split("[")[0].replace(/[0-9]/g, '').slice(0, -1);
    var splitted = room.split("-");
    var sectorId = splitted.pop();
    var checksum = sectorId.match(/\[(.*?)\]/)[1];
    sectorId = parseInt(sectorId.split("[")[0]);
    var letters = splitted.join("").split("");

    return {
      name,
      letters,
      sectorId,
      checksum
    };
  }

  function getMostCommonLetters(letters) {
    var lettersCount = {};

    letters.forEach((letter) => {
      if (lettersCount[letter]) {
        lettersCount[letter]++;
      }
      else {
        lettersCount[letter] = 1;
      }
    });

    var array = [];

    for(var property in lettersCount) {
      if (lettersCount.hasOwnProperty(property)){
         array.push({ "letter": property, "count": lettersCount[property]});
      }
    }

    array.sort(function (a, b) {
      if (a.count === b.count) {
        if(a.letter < b.letter) return -1;
        if(a.letter > b.letter) return 1;
        return 0;
      }
      else {
        if(a.count < b.count) return 1;
        if(a.count > b.count) return -1;
        return 0;
      }

    });

    return array.splice(0, 5);
  }
});

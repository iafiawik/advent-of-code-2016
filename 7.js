var fs = require("fs");
var seven = require('./');

fs.readFile("./inputs/7", function (err, data) {
  if (err) {
    throw err;
  }

  var ips = data.toString().split("\n");
  ips.pop();

  var numberOfIpsSupportingTls = 0;
  var numberOfIpsSupportingSsl = 0;

  ips.forEach((ip) => {
    if (seven.isValidTlsString(ip)) {
      numberOfIpsSupportingTls++;
    }
    if (seven.isValidSslString(ip)) {
      numberOfIpsSupportingSsl++;
    }
  });

  console.log("numberOfIpsSupportingTls: ", numberOfIpsSupportingTls);
  console.log("numberOfIpsSupportingSsl: ", numberOfIpsSupportingSsl);
});
